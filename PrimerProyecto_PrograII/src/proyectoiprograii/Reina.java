/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoiprograii;

/**
 *
 * @author Fabio_2
 */
public class Reina extends Pieza {

    public Reina(Color color) {
        super(color);
    }

    @Override
    public String toString() {
        return "Reina{" + "Color=" + getColor() + '}';
    }
    /**
     * Metodo para mover la Reina en la matriz de casillas
     *
     * @param tablero la matriz de casillas
     * @param x2 Coordenada X de la casilla a la que quiere mover
     * @param y2 Coordenada Y de la casilla a la que quiere mover
     * @param x1 Coordenada X de la pieza seleccionada
     * @param y1 Coordenada Y de la pieza seleccionada
     * @param turno Indica si la pieza es del mismo color del que se esta
     * jugando
     * @return un Boolean para ver si la pieza se puede mover
     */
    @Override
    public boolean mover(Casilla[][] tablero, int x2, int y2, int x1, int y1, Turno turno) {
        return new Torre(getColor()).mover(tablero, x2, y2, x1, y1, turno)
                || new Alfil(getColor()).mover(tablero, x2, y2, x1, y1, turno);
    }

}
