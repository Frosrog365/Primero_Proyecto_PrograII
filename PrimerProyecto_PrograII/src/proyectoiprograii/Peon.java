/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoiprograii;

/**
 *
 * @author Fabio_2
 */
public class Peon extends Pieza {

    public boolean ep_able;

    public Peon(Color color) {
        super(color);
    }

    @Override
    public String toString() {
        return "Peon{" + "Color=" + getColor() + '}';
    }
    /**
     * Metodo abstracto para mover el peon en la matriz de casillas
     *
     * @param tablero la matriz de casillas
     * @param x2 Coordenada X de la casilla a la que quiere mover
     * @param y2 Coordenada Y de la casilla a la que quiere mover
     * @param x1 Coordenada X de la pieza seleccionada
     * @param y1 Coordenada Y de la pieza seleccionada
     * @param turno Indica si la pieza es del mismo color del que se esta
     * jugando
     * @return un Boolean para ver si la pieza se puede mover
     */
    @Override
    public boolean mover(Casilla[][] tablero, int x1, int y1, int x2, int y2, Turno turno) {
        if ((tablero[y1][x1].getPieza() == null) && turno.getColor() == getColor()) {

            if (((y1 - y2 == 1 && x1 == x2) && getColor() == Color.NEGRO) && turno.getColor() == Color.NEGRO) {
                return true;
            } else if (((y1 - y2 == -1 && x1 == x2) && getColor() == Color.BLANCO) && turno.getColor() == Color.BLANCO) {
                return true;
            }
        } else if (tablero[y1][x1].getPieza() != null && (y1 - y2 == 1 && getColor() == Color.NEGRO) && turno.getColor() == Color.NEGRO
                && tablero[y1][x1].getPieza().getColor() != this.getColor() && (x1 - x2 == 1 || x1 - x2 == -1)) {
            return true;
        } else if (tablero[y1][x1].getPieza() != null && (y1 - y2 == -1 && getColor() == Color.BLANCO) && turno.getColor() == Color.BLANCO
                && tablero[y1][x1].getPieza().getColor() != this.getColor() && (x1 - x2 == 1 || x1 - x2 == -1)) {
            return true;
        }
        return false;
    }
}
