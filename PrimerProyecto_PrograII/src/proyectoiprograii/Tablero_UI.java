/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoiprograii;

import java.awt.Image;
import java.awt.event.MouseEvent;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Fabio_2
 */
public class Tablero_UI extends javax.swing.JFrame implements Runnable {

    Tablero tablero;
    int x = 0;
    int y = 0;
    int numeroCasillaX = 0;
    int numeroCasillaY = 0;
    Casilla num;
    int auxX = 0;
    int auxY = 0;
    int click = 0;
    Turno turno;
    Thread t1;
    Thread t2;
    int minBlancos;
    int segBlancos;
    int minNegros;
    int segNegros;
    boolean pausa;
    String jugador1;
    String jugador2;

    public Tablero_UI() {

        initComponents();

    }

    public Tablero_UI(String jugador1, String jugador2) {
        tablero = new Tablero();
        tablero.generarMatriz();
        minBlancos = 5;
        segBlancos = 0;
        minNegros = 5;
        segNegros = 0;

        initComponents();
        setLocationRelativeTo(null);
        setTitle("ChessTuanis v.3.5.5a");
        setResizable(false);
        turno = new Turno();
        turno.setColor(Color.BLANCO);
        t1 = new Thread(this);
        t2 = new Thread(this);
        pintarCasillas();
        t1.start();
        t1.suspend();
        t2.start();
        t2.suspend();
        pausa = false;
        this.jugador1 = jugador1;
        this.jugador2 = jugador2;
        rifa();
        Pausarbtn.setEnabled(false);
        empatarbtn.setEnabled(false);
        Rendirsebtn.setEnabled(false);

    }

    /**
     * Metodo que permite correr el tiempo del lado blanco
     */
    public void correrTiempoBlanco() {
        if (segBlancos < 10 && segBlancos != -1) {
            lblTextoBlanco.setText(minBlancos + ":0" + segBlancos);
            segBlancos--;
        } else if (segBlancos >= 0 && segBlancos >= 10) {
            lblTextoBlanco.setText(minBlancos + ":" + segBlancos);
            segBlancos--;
        } else if (segBlancos == -1) {
            segBlancos = 59;
            minBlancos--;
            lblTextoBlanco.setText(minBlancos + ":" + segBlancos);
            segBlancos--;
        }

        if (minBlancos == 0 && segBlancos
                == 0) {
            JOptionPane.showMessageDialog(null, "Se le ha acabado el tiempo, ha perdido");
            t1.suspend();
            t2.suspend();
        }
    }

    /**
     * Metodo que permite correr el tiempo del lado negro
     */
    public void correrTiempoNegro() {
        if (segNegros < 10 && segNegros != -1) {
            lblTiempoNegro.setText(minNegros + ":0" + segNegros);
            segNegros--;
        } else if (segNegros >= 0 && segNegros >= 10) {
            lblTiempoNegro.setText(minNegros + ":" + segNegros);
            segNegros--;
        } else if (segNegros == -1) {
            segNegros = 59;
            minNegros--;
            lblTiempoNegro.setText(minNegros + ":" + segNegros);
            segNegros--;
        }
        if (minNegros == 0 && segNegros == 0) {
            JOptionPane.showMessageDialog(null, "Se le ha acabado el tiempo, ha perdido");
            t1.suspend();
            t2.suspend();
        }
    }

    /**
     * Metodo que funciona para poder pintar las casillas de 8x8
     */
    public void pintarCasillas() {

        JLabel casilla;
        int numColor = 1;
        java.awt.Color blanco = java.awt.Color.WHITE;
        java.awt.Color negro = java.awt.Color.BLACK;

        click = 0;
        for (int casillasY = 0; casillasY < 8; casillasY++) {
            for (int casillasX = 0; casillasX < 8; casillasX++) {
                casilla = new JLabel();
                casilla.setBounds(x, y, 75, 75);
                casilla.setOpaque(true);
                casilla.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, false));
                if (numColor == 1) {
                    casilla.setBackground(blanco);
                    numColor = 0;
                } else if (numColor == 0) {
                    casilla.setBackground(negro);
                    numColor = 1;
                }
                jPanel1.add(casilla);
                agregarAccion(casilla, numeroCasillaY, numeroCasillaX);
                montarPieza(casilla, numeroCasillaY, numeroCasillaX);
                numeroCasillaX += 1;
                x += 75;

            }
            if (numColor == 1) {
                numColor = 0;
            } else if (numColor == 0) {
                numColor = 1;
            }
            numeroCasillaX = 0;
            while (numeroCasillaY < 7) {
                numeroCasillaY++;
                break;
            }

            x = 0;
            y += 75;

        }
        x = 0;
        y = 0;
        numeroCasillaX = 0;
        numeroCasillaY = 0;
        lblTurno.setText(turno.getColor().toString());

    }

    /**
     * Metodo que pinta la pieza dependiendo de lo que esté en la matriz
     *
     * @param casilla label que reprensenta en la matriz
     * @param y coordenada y de la matriz
     * @param x coordenada x de la matriz
     */
    public void montarPieza(JLabel casilla, int y, int x) {
        ImageIcon icon = null;
        if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Torre && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.NEGRO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\Black R.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Caballo && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.NEGRO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\Black N.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));

        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Alfil && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.NEGRO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\Black B.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Reina && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.NEGRO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\Black Q.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Rey && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.NEGRO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\Black K.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Peon && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.NEGRO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\Black P.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Peon && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.BLANCO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\White P.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() == null) {
            casilla.setIcon(null);
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Torre && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.BLANCO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\White R.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));

        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Caballo && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.BLANCO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\White N.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Alfil && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.BLANCO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\White B.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Reina && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.BLANCO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\White Q.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        } else if (tablero.getMatrizCasillas()[y][x].getPieza() instanceof Rey && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.BLANCO) {
            casilla.setIcon(new ImageIcon(new ImageIcon("src\\Imagenes\\White K.png").getImage().getScaledInstance(casilla.getWidth(), casilla.getHeight(), Image.SCALE_DEFAULT)));
        }
    }

    /**
     * Metodo que permite agregar la accion del click al Label
     *
     * @param casilla Label de la matriz
     * @param y coordenada de la matriz
     * @param x coordenada de la matriz
     */
    public void agregarAccion(JLabel casilla, int y, int x) {
        casilla.addMouseListener(new java.awt.event.MouseListener() {
            @Override
            public void mouseClicked(MouseEvent me) {
                if (pausa) {
                    if (tablero.getMatrizCasillas()[y][x].getPieza() != null
                            && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == turno.getColor() && click == 0) {
                        casilla.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(255, 255, 0), 2, false));

                    } else {
                        casilla.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, false));

                    }

                    if (tablero.getMatrizCasillas()[y][x].getPieza() != null && click == 0) {

                        auxX = x;
                        auxY = y;
                        num = tablero.getMatrizCasillas()[y][x];
                        click = 1;

                    } else if (tablero.getMatrizCasillas()[y][x].getPieza() == null && click == 1) {

                        if (tablero.getMatrizCasillas()[auxY][auxX].getPieza().mover(tablero.getMatrizCasillas(), x, y, auxX, auxY, turno)) {
                            tablero.getMatrizCasillas()[y][x].setPieza(num.getPieza());
                            tablero.getMatrizCasillas()[auxY][auxX].setPieza(null);
                            if (turno.getColor() == Color.BLANCO) {
                                turno.setColor(Color.NEGRO);

                                t1.suspend();
                                t2.resume();
                                repaint();
                            } else if (turno.getColor() == Color.NEGRO) {
                                turno.setColor(Color.BLANCO);
                                t2.suspend();
                                t1.resume();
                                repaint();
                            }
                            jPanel1.removeAll();
                            click = 0;
                            pintarCasillas();
                            repaint();

                        } else {
                            click = 0;
                        }
                    } else if ((tablero.getMatrizCasillas()[y][x].getPieza() instanceof Rey) && click == 1
                            && tablero.getMatrizCasillas()[auxY][auxX].getPieza().mover(tablero.getMatrizCasillas(), x, y, auxX, auxY, turno)) {
                        if (tablero.getMatrizCasillas()[auxY][auxX].getPieza().getColor() == Color.BLANCO && tablero.getMatrizCasillas()[y][x].getPieza().getColor() == Color.NEGRO) {
                            tablero.getMatrizCasillas()[y][x].setPieza(num.getPieza());
                            tablero.getMatrizCasillas()[auxY][auxX].setPieza(null);
                            jPanel1.removeAll();
                            pintarCasillas();
                            repaint();
                            t1.suspend();
                            t2.suspend();
                            JOptionPane.showMessageDialog(null, "Ganó " + jugador1 + "!!", "GAME OVER", 0);

                            resetearTablero();

                        } else {
                            tablero.getMatrizCasillas()[y][x].setPieza(num.getPieza());
                            tablero.getMatrizCasillas()[auxY][auxX].setPieza(null);
                            jPanel1.removeAll();
                            pintarCasillas();
                            repaint();
                            t1.suspend();
                            t2.suspend();
                            JOptionPane.showMessageDialog(null, "Ganó " + jugador2 + "!!", "GAME OVER", 0);

                            resetearTablero();
                        }

                    } else if (tablero.getMatrizCasillas()[y][x].getPieza() != null && click == 1) {
                        if (tablero.getMatrizCasillas()[auxY][auxX].getPieza().mover(tablero.getMatrizCasillas(), x, y, auxX, auxY, turno)) {
                            tablero.getMatrizCasillas()[y][x].setPieza(num.getPieza());
                            tablero.getMatrizCasillas()[auxY][auxX].setPieza(null);
                            num.setPieza(null);
                            if (turno.getColor() == Color.BLANCO) {
                                turno.setColor(Color.NEGRO);
                                t1.suspend();
                                t2.resume();
                            } else if (turno.getColor() == Color.NEGRO) {
                                turno.setColor(Color.BLANCO);
                                t1.resume();
                                t2.suspend();
                            }

                            jPanel1.removeAll();
                            pintarCasillas();
                            click = 0;
                            repaint();
                        } else {
                            click = 0;
                            repaint();
                        }
                    }

                }
            }

            @Override

            public void mousePressed(MouseEvent me) {
            }

            @Override
            public void mouseReleased(MouseEvent me) {
            }

            @Override
            public void mouseEntered(MouseEvent me) {
            }

            @Override
            public void mouseExited(MouseEvent me) {
            }
        }
        );
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        btnNuevoJuego = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        lblTurno = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblTextoBlanco = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lblTiempoNegro = new javax.swing.JLabel();
        txtNombre1 = new javax.swing.JLabel();
        txtNombre2 = new javax.swing.JLabel();
        salirbtn = new javax.swing.JButton();
        empatarbtn = new javax.swing.JButton();
        Rendirsebtn = new javax.swing.JButton();
        Pausarbtn = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 598, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 598, Short.MAX_VALUE)
        );

        btnNuevoJuego.setFont(new java.awt.Font("Segoe UI Symbol", 1, 24)); // NOI18N
        btnNuevoJuego.setText("Nuevo Juego");
        btnNuevoJuego.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoJuegoActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel1.setText("Turno: ");

        lblTurno.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N

        jLabel2.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel2.setText("   a      b     c     d      e      f      g     h");

        jLabel3.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel3.setText("7");
        jLabel3.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel4.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel4.setText("8");
        jLabel4.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel5.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("6");
        jLabel5.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel6.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("5");
        jLabel6.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel7.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel7.setText("4");
        jLabel7.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel8.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel8.setText("3");
        jLabel8.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel9.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel9.setText("2");
        jLabel9.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel10.setFont(new java.awt.Font("Nirmala UI", 1, 36)); // NOI18N
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel10.setText("1");
        jLabel10.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        jLabel11.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel11.setText("Jug. Blanco");

        lblTextoBlanco.setFont(new java.awt.Font("alarm clock", 1, 36)); // NOI18N
        lblTextoBlanco.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTextoBlanco.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel13.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N
        jLabel13.setText("Jug. Negro");

        lblTiempoNegro.setFont(new java.awt.Font("alarm clock", 1, 36)); // NOI18N
        lblTiempoNegro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblTiempoNegro.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        txtNombre1.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N

        txtNombre2.setFont(new java.awt.Font("Century Gothic", 1, 24)); // NOI18N

        salirbtn.setFont(new java.awt.Font("Segoe UI Symbol", 1, 24)); // NOI18N
        salirbtn.setText("Salir");
        salirbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                salirbtnActionPerformed(evt);
            }
        });

        empatarbtn.setFont(new java.awt.Font("Segoe UI Symbol", 1, 24)); // NOI18N
        empatarbtn.setText("Empatar");
        empatarbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                empatarbtnActionPerformed(evt);
            }
        });

        Rendirsebtn.setFont(new java.awt.Font("Segoe UI Symbol", 1, 24)); // NOI18N
        Rendirsebtn.setText("Rendirse");
        Rendirsebtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                RendirsebtnActionPerformed(evt);
            }
        });

        Pausarbtn.setFont(new java.awt.Font("Segoe UI Symbol", 1, 24)); // NOI18N
        Pausarbtn.setText("Pausar Partida");
        Pausarbtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PausarbtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(jLabel2))
                .addGap(69, 69, 69)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(salirbtn)
                            .addGap(18, 18, 18)
                            .addComponent(empatarbtn)
                            .addGap(18, 18, 18)
                            .addComponent(Rendirsebtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel11)
                            .addGap(18, 18, 18)
                            .addComponent(txtNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(lblTextoBlanco, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel13)
                            .addGap(18, 18, 18)
                            .addComponent(txtNombre2, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(lblTiempoNegro, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnNuevoJuego)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(Pausarbtn)))
                .addContainerGap(57, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(btnNuevoJuego)
                                    .addComponent(Pausarbtn))
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 48, Short.MAX_VALUE)
                                    .addComponent(lblTurno, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(39, 39, 39)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtNombre1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTextoBlanco, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(jLabel4)
                                .addGap(34, 34, 34)
                                .addComponent(jLabel3)
                                .addGap(30, 30, 30)
                                .addComponent(jLabel5)))
                        .addGap(30, 30, 30)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(30, 30, 30)
                                .addComponent(jLabel7)
                                .addGap(30, 30, 30)
                                .addComponent(jLabel8)
                                .addGap(27, 27, 27)
                                .addComponent(jLabel9)
                                .addGap(24, 24, 24)
                                .addComponent(jLabel10))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(txtNombre2, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblTiempoNegro, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(salirbtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(empatarbtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(Rendirsebtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoJuegoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoJuegoActionPerformed
        resetearTablero();
        t1.resume();
        Pausarbtn.setEnabled(true);
        empatarbtn.setEnabled(true);
        Rendirsebtn.setEnabled(true);
        pausa = true;

    }//GEN-LAST:event_btnNuevoJuegoActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed

    }//GEN-LAST:event_formWindowClosed

    private void salirbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_salirbtnActionPerformed
        VentanaInicioRifa vIR = new VentanaInicioRifa();
        vIR.setVisible(true);
        dispose();
    }//GEN-LAST:event_salirbtnActionPerformed

    private void empatarbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_empatarbtnActionPerformed
        t1.suspend();
        t2.suspend();
        int si = JOptionPane.showConfirmDialog(null, "Su contrincario desea empatar, también lo desea usted?", "Empate?", JOptionPane.YES_NO_OPTION);
        if (si == JOptionPane.OK_OPTION) {
            JOptionPane.showMessageDialog(null, "Partida Empatada");
            salir();
        }
    }//GEN-LAST:event_empatarbtnActionPerformed

    private void PausarbtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PausarbtnActionPerformed
        if (Pausarbtn.getText().equals("Pausar Partida")) {
            Pausarbtn.setText("Reanudar Partida");
            t1.suspend();
            t2.suspend();
            pausa = false;

        } else {
            Pausarbtn.setText("Pausar Partida");
            if (turno.getColor() == Color.BLANCO) {
                t1.resume();
            } else {
                t2.resume();
            }
            pausa = true;

        }
    }//GEN-LAST:event_PausarbtnActionPerformed

    private void RendirsebtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_RendirsebtnActionPerformed
        t1.suspend();
        t2.suspend();
        int si = JOptionPane.showConfirmDialog(this, "¿De verdad desea rendirse?", "¿Rendirse?", JOptionPane.YES_NO_OPTION);
        if (si == JOptionPane.YES_OPTION && turno.getColor() == Color.BLANCO) {
            JOptionPane.showMessageDialog(null, "Ganó " + jugador2 + "!!", "GAME OVER", 0);
            salir();
        } else if (si == JOptionPane.YES_OPTION && turno.getColor() == Color.NEGRO) {
            JOptionPane.showMessageDialog(null, "Ganó " + jugador1 + "!!", "GAME OVER", 0);
            salir();
        }
    }//GEN-LAST:event_RendirsebtnActionPerformed
    /**
     * Metodo que permite resetear el tablero
     */
    public void resetearTablero() {
        turno.setColor(Color.BLANCO);
        click = 0;
        minBlancos = 5;
        segBlancos = 0;
        minNegros = 5;
        segNegros = 0;
        jPanel1.removeAll();
        tablero.generarMatriz();
        pintarCasillas();
        lblTextoBlanco.setText(minBlancos + ":0" + segBlancos);
        lblTiempoNegro.setText(minNegros + ":0" + segNegros);
        repaint();
        t1.suspend();
        t2.suspend();
        Pausarbtn.setEnabled(false);
        empatarbtn.setEnabled(false);
        Rendirsebtn.setEnabled(false);
        pausa = false;
    }

    /**
     * Metodo que hace la rifa para ver quien es el jugador blanco y quien el
     * jugador negro
     */
    public void rifa() {
        int randomNum = 0 + (int) (Math.random() * 2);
        String[] nombres = new String[2];
        nombres[0] = jugador1;
        nombres[1] = jugador2;
        String jugadorPrimero = nombres[randomNum];
        String jugadorSegundo;
        txtNombre1.setText(jugadorPrimero);
        if (jugadorPrimero.equals(nombres[0])) {
            jugadorSegundo = nombres[1];
        } else {
            jugadorSegundo = nombres[0];
        }
        txtNombre2.setText(jugadorSegundo);

    }

    /**
     * Se sale de la ventana completamente
     */
    public void salir() {
        VentanaInicioRifa vIR = new VentanaInicioRifa();
        vIR.setVisible(true);
        dispose();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Tablero_UI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Tablero_UI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Tablero_UI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Tablero_UI.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Tablero_UI().setVisible(true);
            }
        });

    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Pausarbtn;
    private javax.swing.JButton Rendirsebtn;
    private javax.swing.JButton btnNuevoJuego;
    private javax.swing.JButton empatarbtn;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblTextoBlanco;
    private javax.swing.JLabel lblTiempoNegro;
    private javax.swing.JLabel lblTurno;
    private javax.swing.JButton salirbtn;
    private javax.swing.JLabel txtNombre1;
    private javax.swing.JLabel txtNombre2;
    // End of variables declaration//GEN-END:variables

    /**
     * Metodo que permite correr los tiempos
     */
    @Override
    public void run() {
        Thread ct = Thread.currentThread();
        try {
            while (ct == t1) {
                correrTiempoBlanco();
                Thread.sleep(1000);
            }
        } catch (Exception e) {
        }
        try {
            while (ct == t2) {
                correrTiempoNegro();
                Thread.sleep(1000);
            }
        } catch (Exception e) {

        }
    }

}
