/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoiprograii;

/**
 *
 * @author Fabio_2
 */
public class Tablero {

    int[][] matriz = {{1, 2, 3, 4, 5, 3, 2, 1},
    {6, 6, 6, 6, 6, 6, 6, 6},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {0, 0, 0, 0, 0, 0, 0, 0},
    {13, 13, 13, 13, 13, 13, 13, 13},
    {11, 7, 8, 9, 10, 8, 7, 11}
    };
    private Casilla[][] matrizCasillas;

    ;

    public Tablero() {
        this.matrizCasillas = matrizCasillas;
    }

    /**
     * Metodo para resetear la matriz
     */
    public void reset() {
        this.matriz = new int[][]{{1, 2, 3, 4, 5, 3, 2, 1},
        {6, 6, 6, 6, 6, 6, 6, 6},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {0, 0, 0, 0, 0, 0, 0, 0},
        {13, 13, 13, 13, 13, 13, 13, 13},
        {11, 7, 8, 9, 10, 8, 7, 11}
        };
    }

    /**
     * Metodo para poder crear una matriz de casillas a base de la matriz
     * numerica
     *
     * @return Una matriz de casillas con las piezas correspondientes
     */
    public Casilla[][] generarMatriz() {
        this.matrizCasillas = new Casilla[8][8];
        for (int y = 0; y < matriz.length; y++) {
            for (int x = 0; x < matriz[y].length; x++) {
                switch (this.matriz[y][x]) {
                    case 1:
                        this.matrizCasillas[y][x] = new Casilla(new Torre(Color.NEGRO));
                        break;
                    case 2:
                        this.matrizCasillas[y][x] = new Casilla(new Caballo(Color.NEGRO));
                        break;
                    case 3:
                        this.matrizCasillas[y][x] = new Casilla(new Alfil(Color.NEGRO));
                        break;
                    case 4:
                        this.matrizCasillas[y][x] = new Casilla(new Reina(Color.NEGRO));
                        break;
                    case 5:
                        this.matrizCasillas[y][x] = new Casilla(new Rey(Color.NEGRO));
                        break;
                    case 6:
                        this.matrizCasillas[y][x] = new Casilla(new Peon(Color.NEGRO));
                        break;
                    case 0:
                        this.matrizCasillas[y][x] = new Casilla();
                        break;
                    case 13:
                        this.matrizCasillas[y][x] = new Casilla(new Peon(Color.BLANCO));
                        break;
                    case 11:
                        this.matrizCasillas[y][x] = new Casilla(new Torre(Color.BLANCO));
                        break;
                    case 7:
                        this.matrizCasillas[y][x] = new Casilla(new Caballo(Color.BLANCO));
                        break;
                    case 8:
                        this.matrizCasillas[y][x] = new Casilla(new Alfil(Color.BLANCO));
                        break;
                    case 9:
                        this.matrizCasillas[y][x] = new Casilla(new Reina(Color.BLANCO));
                        break;
                    case 10:
                        this.matrizCasillas[y][x] = new Casilla(new Rey(Color.BLANCO));
                        break;
                    default:
                        break;
                }

            }
        }
        return matrizCasillas;

    }

    public Casilla[][] getMatrizCasillas() {
        return matrizCasillas;
    }
}
