/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoiprograii;

/**
 *
 * @author Fabio_2
 */
public enum Color {
    BLANCO,
    NEGRO;

    private Color() {
    }

    public static Color getBLANCO() {
        return BLANCO;
    }

    public static Color getNEGRO() {
        return NEGRO;
    }

    /**
     * Metodo toString para el color
     * @return Un string con el color
     */
    @Override
    public String toString() {
       if(this == BLANCO){
           return "Blanco";
       }
       else{
           return "Negro";
       }
    }
    public boolean getColor(){
        return true;
    }
    
    
    
    

}
