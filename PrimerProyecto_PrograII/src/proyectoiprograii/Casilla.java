/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoiprograii;

/**
 *
 * @author Fabio_2
 */
public class Casilla {

    private Pieza pieza;
    private Coordenada coordenada;

    public Casilla(Pieza pieza) {

        this.pieza = pieza;
        this.coordenada = new Coordenada();
    }

    public Casilla() {
    }

    public Pieza getPieza() {
        return pieza;
    }

    public void setPieza(Pieza pieza) {
        this.pieza = pieza;
    }

    @Override
    public String toString() {
        return "Casilla{" + "pieza=" + pieza + '}';
    }

}
