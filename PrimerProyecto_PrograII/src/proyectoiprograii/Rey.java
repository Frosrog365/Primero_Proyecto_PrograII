/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectoiprograii;

/**
 *
 * @author Fabio_2
 */
public class Rey extends Pieza {

    public Rey(Color color) {
        super(color);
    }

    @Override
    public String toString() {
        return "Rey{" + "Color=" + getColor() + '}';
    }

    /**
     * Metodo abstracto para mover el rey en la matriz de casillas
     *
     * @param tablero la matriz de casillas
     * @param x2 Coordenada X de la casilla a la que quiere mover
     * @param y2 Coordenada Y de la casilla a la que quiere mover
     * @param x1 Coordenada X de la pieza seleccionada
     * @param y1 Coordenada Y de la pieza seleccionada
     * @param turno Indica si la pieza es del mismo color del que se esta
     * jugando
     * @return un Boolean para ver si la pieza se puede mover
     */
    @Override
    public boolean mover(Casilla[][] tablero, int x2, int y2, int x1, int y1, Turno turno) {
        if (this.getColor() == turno.getColor() && (tablero[y2][x2].getPieza() == null
                || this.getColor() != tablero[y2][x2].getPieza().getColor())) {
            if ((x2 == x1 && y2 == y1)||(Math.abs(x2 - x1) > 1 || Math.abs(y2 - y1) > 1) ) {
                return false;
            }
            return true;
        }
        return false;
    }

}
